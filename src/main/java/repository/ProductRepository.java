package repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.query.Param;


import model.Produit;

public interface ProductRepository extends JpaRepository<Produit,Long>{
	
	Produit findByNameAndPrice // respecter la convention de nommage findBy....
	(
			@Param("name") String name,
			@Param("price") double price
			
			);
	

}
