package dao;

import java.util.List;

public interface DAO<T> {

	void create(T obj);
	T findById(int id);
	List<T> findAll();
	void delete(int id);
	void update(T obj);
}
