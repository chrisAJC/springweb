package dao;

import java.util.*;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import model.Produit;
import util.Context;

public class DAOProduit implements DAO<Produit> {

	@Override
	public void create(Produit obj) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        em.persist(obj);
        em.getTransaction().commit();
        em.close();
        Context.destroy();		
	}

	@Override
	public Produit findById(int id) {
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        Produit obj = em.find(Produit.class, id);
        em.close();
        Context.destroy();
		return obj;
	}

	@Override
	public List<Produit> findAll() {

		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        Query query = em.createQuery("from Produit");
        List<Produit> objs = query.getResultList();
        em.close();
       	Context.destroy();
		return objs;
		
		
	}

	@Override
	public void delete(int id) {
		Produit obj = findById(id);
		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
      	em.remove(em.merge(obj));
        em.getTransaction().commit();
        em.close();
        Context.destroy();
		
		
	}

	@Override
	public void update(Produit obj) {

		EntityManager em = Context.getInstance().getEntityManagerFactory().createEntityManager();
        em.getTransaction().begin();
        em.merge(obj);
        em.getTransaction().commit();
        em.close();
        Context.destroy();
		
	}

}
