package model;

public class Produit {
	
	private int id;
	private double prix;
	private String description;
	
	public Produit(double prix, String describe,int id)
	{
		this.prix = prix;
		this.description = describe;
		this.id = id;
	}
	
	public Produit() {
		
	}
	
	public double getPrix() {
		return prix;
	}
	public void setPrix(double prix) {
		this.prix = prix;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "Produit [id=" + id + ", prix=" + prix + ", description=" + description + "]";
	}

	
	
	
	
	
}
