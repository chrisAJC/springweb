package producer;

import java.util.List;

import javax.annotation.PostConstruct;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import model.Produit;
import service.ProductService;

@Component
public class ProductProducer {

	private ProductService productService;
	private Log logger = LogFactory.getLog(ProductProducer.class);
	
	@Autowired
	public ProductProducer(ProductService ps) {
		this.productService = ps;
	}
	
	
	@PostConstruct     //s execute apres le constructeur , principe du IoC
	public void produceData() {
		
		findProducts();
		addOneProduct();
		findProducts();
		
	}
	
	
	private void addOneProduct() {
		logger.info("Adding new product now!");
		productService.addProduct(new Produit(100,"iphone",1));
	}
	
	private void findProducts() {
		logger.info("trying to find all products.");
		List<Produit> allProducts = productService.getAllProducts();
		if(allProducts.isEmpty()) {
			logger.info("--No products found--");
		}else {
			for(Produit foundProduct : allProducts) {
				logger.info(String.format("product with id %d and name %s found", foundProduct.getId(),foundProduct.getDescription()));
			}
		}
	}
	
}
