package repository;

import static org.fest.assertions.Assertions.assertThat;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class PRTest {

	@Autowired
	private ProductRepository pr;
	
	@Test
	public void testFindAll_andProducerWorked() {
		assertThat(pr.findAll()).hasSize(1);
	}
	
	@Test
	public void findByNameAndPrice_andNoneFound() {
		assertThat(pr.findByNameAndPrice("android",10)).isNull();
	}
	
	@Test
	public void findByNameAndPrice_andFoundOne() {
		assertThat(pr.findByNameAndPrice("Iphone",10)).isNotNull();
	}
	
}
