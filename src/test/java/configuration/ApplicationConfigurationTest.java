package configuration;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.test.context.junit4.SpringRunner;

import com.example.springWeb.com.example.springWeb.SpringWebApplication;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT, classes = SpringWebApplication.class)
public class ApplicationConfigurationTest {

	@Test
	public void bootstrapsApplication() {
		new AnnotationConfigApplicationContext(SpringWebApplication.class);
	}
	
	
	
}
